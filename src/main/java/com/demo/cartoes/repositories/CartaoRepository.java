package com.demo.cartoes.repositories;

import com.demo.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Cartao findByNumero(String numero);
}
