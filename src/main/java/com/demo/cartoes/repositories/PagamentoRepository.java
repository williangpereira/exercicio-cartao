package com.demo.cartoes.repositories;

import com.demo.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Pagamento findById(int id);
}
