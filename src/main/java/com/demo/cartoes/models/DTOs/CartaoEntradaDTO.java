package com.demo.cartoes.models.DTOs;


import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class CartaoEntradaDTO {

    private String numero;
    private int clienteId;

    public CartaoEntradaDTO() {}

    public CartaoEntradaDTO(String numero, int clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

}
