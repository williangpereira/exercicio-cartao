package com.demo.cartoes.models.DTOs;


public class CartaoRespostaDTO {

    private int id;

    private String numero;

    private int clienteId;

    public CartaoRespostaDTO() {}

    public CartaoRespostaDTO(int id, String numero, int clienteId) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
