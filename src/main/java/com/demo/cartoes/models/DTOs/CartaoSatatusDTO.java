package com.demo.cartoes.models.DTOs;

public class CartaoSatatusDTO {

    public boolean ativo;

    public CartaoSatatusDTO() {}

    public CartaoSatatusDTO(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
