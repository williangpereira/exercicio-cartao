package com.demo.cartoes.services;

import com.demo.cartoes.models.Cliente;
import com.demo.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Iterable<Cliente> listarClientes(){
        Iterable<Cliente> listaDeClientes = clienteRepository.findAll();
        return listaDeClientes;
    }

    public Optional<Cliente> buscarPorID(int id){
        if(clienteRepository.existsById(id)) {
            Optional<Cliente> clientePodID = clienteRepository.findById(id);
            return clientePodID;
        }
        throw new RuntimeException("Cliente não encontrado");
    }

   public Cliente atualizarCliente(int id, Cliente cliente){
        if(clienteRepository.existsById(id)){
            cliente.setId(id);
            Cliente clienteObjeto = clienteRepository.save(cliente);
            return clienteObjeto;
        }
        throw new RuntimeException("Cliente não encontrado!");
    }

}
