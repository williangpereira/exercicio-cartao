package com.demo.cartoes.services;

import com.demo.cartoes.models.Pagamento;
import com.demo.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    public Pagamento criarPagamento(Pagamento pagamento){
        Pagamento pagamentoRealizado = pagamentoRepository.save(pagamento);
        return pagamentoRealizado;
    }

    public Pagamento BuscarPagamentoPorId(int id){
        Pagamento pagamentoPorID = pagamentoRepository.findById(id);
        return pagamentoPorID;
    }

    public Iterable<Pagamento> listarPagamentos(){
        Iterable<Pagamento> listaDePagamentos = pagamentoRepository.findAll();
        return listaDePagamentos;
    }

}
