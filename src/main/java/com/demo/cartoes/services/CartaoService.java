package com.demo.cartoes.services;

import com.demo.cartoes.models.Cartao;
import com.demo.cartoes.models.DTOs.CartaoEntradaDTO;
import com.demo.cartoes.models.DTOs.CartaoRespostaDTO;
import com.demo.cartoes.models.DTOs.CartaoSatatusDTO;
import com.demo.cartoes.repositories.CartaoRepository;
import com.demo.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public Cartao criarCartao(CartaoEntradaDTO cartaoEntradaDTO){

        if (clienteRepository.existsById(cartaoEntradaDTO.getClienteId())){
            Cartao cartaoCriado = new Cartao();
            cartaoCriado.setNumero(cartaoEntradaDTO.getNumero());
            cartaoCriado.setClienteId(cartaoEntradaDTO.getClienteId());
            cartaoCriado.setAtivo(false);
            cartaoRepository.save(cartaoCriado);

            return cartaoCriado;
        }
        throw new RuntimeException("Cliente não encontrado, cartão não pode ser criado");
    }

    public CartaoRespostaDTO exibirCartao(Cartao cartao){

        CartaoRespostaDTO cartaoRespostaDTO = new CartaoRespostaDTO();
        cartaoRespostaDTO.setClienteId(cartao.getClienteId());
        cartaoRespostaDTO.setNumero(cartao.getNumero());
        cartaoRepository.save(cartao);

        return cartaoRespostaDTO;
    }

    public CartaoRespostaDTO buscarPorNumero(String numero){

        Cartao cartaoRetorno = cartaoRepository.findByNumero(numero);

        CartaoRespostaDTO cartaoRespostaDTO = new CartaoRespostaDTO();
        cartaoRespostaDTO.setId(cartaoRetorno.getId());
        cartaoRespostaDTO.setNumero(cartaoRetorno.getNumero());
        cartaoRespostaDTO.setClienteId(cartaoRetorno.getClienteId());

        return cartaoRespostaDTO;

    }

    public Iterable<Cartao> listarCartoes(){
        Iterable<Cartao> listaDeCartoes = cartaoRepository.findAll();
        return listaDeCartoes;
    }

    public Cartao ativarCartao(String numero, CartaoSatatusDTO cartaoSatatusDTO){

        Cartao cartaoStatus = cartaoRepository.findByNumero(numero);

        cartaoStatus.setAtivo(cartaoSatatusDTO.isAtivo());
        cartaoRepository.save(cartaoStatus);

        return cartaoStatus;
    }

}
