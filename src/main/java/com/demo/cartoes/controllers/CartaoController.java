package com.demo.cartoes.controllers;

import com.demo.cartoes.models.Cartao;
import com.demo.cartoes.models.DTOs.CartaoEntradaDTO;
import com.demo.cartoes.models.DTOs.CartaoRespostaDTO;
import com.demo.cartoes.models.DTOs.CartaoSatatusDTO;
import com.demo.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao criarCartao(@RequestBody CartaoEntradaDTO cartaoEntradaDTO){
        Cartao cartaoCriado = cartaoService.criarCartao(cartaoEntradaDTO);
        return cartaoCriado;
    }

    @GetMapping("/{numero}")
    public CartaoRespostaDTO retornarCartao(@PathVariable(name = "numero") String numero){
        CartaoRespostaDTO cartaoRetorno = cartaoService.buscarPorNumero(numero);
        return cartaoRetorno;
    }

    @GetMapping
    public Iterable<Cartao> listarTodosCartoes(){
        Iterable<Cartao> listaDeCartoes = cartaoService.listarCartoes();
        return listaDeCartoes;
    }

    @PatchMapping("/{numero}")
    public Cartao ativarCartao(@PathVariable (name = "numero") String numero, @RequestBody CartaoSatatusDTO cartaoSatatusDTO){
        Cartao status = cartaoService.ativarCartao(numero, cartaoSatatusDTO);
        return status;
    }
}
