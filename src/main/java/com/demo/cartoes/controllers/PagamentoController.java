package com.demo.cartoes.controllers;

import com.demo.cartoes.models.Pagamento;
import com.demo.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    public Pagamento criarPagamento(@RequestBody Pagamento pagamento){
        Pagamento pagamentoObjeto = pagamentoService.criarPagamento(pagamento);
        return pagamentoObjeto;
    }

    @GetMapping
    public Iterable<Pagamento> listarPagamentos(){
        Iterable<Pagamento> listaPagamentos = pagamentoService.listarPagamentos();
        return listaPagamentos;
    }

    @GetMapping("/{id}")
    public Pagamento buscarPorId(@PathVariable(name = "id") int id){
        try{
            Pagamento pagamentoPorId = pagamentoService.BuscarPagamentoPorId(id);
            return pagamentoPorId;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }
}
