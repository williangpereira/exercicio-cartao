package com.demo.cartoes.controllers;

import com.demo.cartoes.models.Cliente;
import com.demo.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody Cliente cliente){
        Cliente clienteCadastrado = clienteService.cadastrarCliente(cliente);
        return clienteCadastrado;
    }

    @GetMapping
    public Iterable<Cliente> listarClientes(){
        Iterable<Cliente> listaDeClientes = clienteService.listarClientes();
        return listaDeClientes;
    }

    @GetMapping("/{id}")
    public Optional<Cliente> clientePorID(@PathVariable(name = "id") int id){
        try{
            Optional<Cliente> clienteOptional = clienteService.buscarPorID(id);
            return clienteOptional;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Cliente atualizarCliente(@PathVariable(name="id") int id, @RequestBody Cliente cliente){
        try{
            Cliente clienteObjeto = clienteService.atualizarCliente(id, cliente);
            return clienteObjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
